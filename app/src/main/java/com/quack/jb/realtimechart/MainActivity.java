package com.quack.jb.realtimechart;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.SeekBar;
import android.widget.Toast;

public class MainActivity extends Activity implements SeekBar.OnSeekBarChangeListener {

    private final Handler mHandler = new Handler(Looper.getMainLooper()); // To schedule runnables
    private Runnable mTimer; // Interface to execute a block of code with a thread different from UI thread
    private double time=0;
    private double fs=512;
    private Chart chart;
    private SeekBar timeSB,ampSB;
    private Integer integer; // test
    private String msg; //test
    private int maxTime=5;
    private int maxAmp=2;
    private boolean reset=false;

    /* START: SeekBar.OnSeekBarChangeListener Methods */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        integer = new Integer(progress+1);

        switch (seekBar.getId()){
            case R.id.timeSeekBar:
                msg = "time:" + integer.toString();
                maxTime = progress+1;
                break;
            case R.id.ampSeekBar:
                msg = "amplitude:" + integer.toString();
                maxAmp = progress+1;
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mTimer);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        chart.setViewPort(0,maxTime,-maxAmp,maxAmp,fs);
        reset=true;
        mHandler.postDelayed(mTimer,200);
    }

    /* END: SeekBar.OnSeekBarChangeListener */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timeSB = findViewById(R.id.timeSeekBar);
        ampSB = findViewById(R.id.ampSeekBar);
        timeSB.setOnSeekBarChangeListener(this);
        ampSB.setOnSeekBarChangeListener(this);
        initGraph();
    }



    private void initGraph(){
        chart = (Chart) getFragmentManager().findFragmentById(R.id.chart_fragment);
        chart.initGraph(0, 2, -1, 1, fs);
        chart.setUpdateEnabled(false); // The time is not updated
    }


    @Override
    protected void onResume() {
        super.onResume();
        // timer
        mTimer = new Runnable() {
            @Override
            public void run() {
                //TODO improve: plot more than one sample.
                for(int i=0;i<fs/10;i++) {
                    chart.singleAppend(Math.sin(2 * Math.PI * 0.7 * time / fs));
                    time++;
                }
                reset=false;
                mHandler.postDelayed(this, (long) (100)); // Will return to run in the set delay
            }
        };
        //First post (Immediately)
        mHandler.post(mTimer);
    }
}


