package com.quack.jb.realtimechart;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.quack.jb.realtimechart.edf.EDF;

import java.util.Random;

/**
 * Created by quack on 15/11/17.
 */

public class RealtimeUpdates extends Fragment {
    private View rootView; // variable de instancia para guardar la vista padre contenedora del fragment
    private final Handler mHandler = new Handler(); // handler para realizar tareas temporizadas
    private Runnable mTimer1; // interfaz para ejecutar "bloques"
    private LineGraphSeries<DataPoint> mSeries1; // vector de DataPoint
    private double graphLastXValue = 1000d;  // ultimo valor. importante que sea correcto para que la graficacion comience en 0


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //obtiene el layout que forma el fragmento
        rootView = inflater.inflate(R.layout.fragment_main, container, false);
        //luego busca los elementos del layout
        GraphView graph = (GraphView) rootView.findViewById(R.id.graph);
        initGraph(graph);

        return rootView;
    }

    public void initGraph(GraphView graph) {
        //parametros del viewport
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(1000);

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(-1);
        graph.getViewport().setMaxY(1);

        // first mSeries is a line
        mSeries1 = new LineGraphSeries<>();
        graph.addSeries(mSeries1);
    }

    /*
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //((MainActivity) activity).onSectionAttached(getArguments().getInt(MainActivity.ARG_SECTION_NUMBER));
    }
    */

    @Override
    public void onResume() {
        super.onResume();

        mTimer1 = new Runnable() {
            @Override
            public void run() {
                // cuando llega al final vuelve al punto inicial
                for(int i=0;i<72;i++) {
                    if (graphLastXValue == 1000) {
                        graphLastXValue = 0;
                        //serie con un solo valor
                        mSeries1.resetData(new DataPoint[]{new DataPoint(graphLastXValue, getSignal())});
                    }
                    graphLastXValue += 1d; // avance
                    double doub = getSignal();
                    //agrega los valores de una cada vez que se ejecuta run()
                    mSeries1.appendData(new DataPoint(graphLastXValue, doub), false, 1000);

                }
                mHandler.postDelayed(this,100);
            }
        };
        //ejecuta el timer
        mHandler.post(mTimer1);
    }

    double mLastRandom = 2;
    private double getSignal() {
        mLastRandom++;
        return Math.sin(mLastRandom*0.5) * 60 ;

    }
//    private double getSignal2() {

  //      return MainActivity.edf.getValue();

    //}


    @Override
    public void onPause() {
        mHandler.removeCallbacks(mTimer1);
        super.onPause();
    }

    private DataPoint[] generateData() {
        int count = 1000;
        DataPoint[] values = new DataPoint[count];
        for (int i=0; i<count; i++) {
            double x = i;
            double y = Math.sin(i*10*3.14);
            DataPoint v = new DataPoint(x, y);
            values[i] = v;
        }
        return values;
    }


}
