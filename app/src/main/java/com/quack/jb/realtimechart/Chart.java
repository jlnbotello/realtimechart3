package com.quack.jb.realtimechart;

import android.app.Fragment;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

/**
 * Created by quack on 28/11/17.
 */

public class Chart extends Fragment {
    private View rootView;
    private GraphView graph;
    private LineGraphSeries<DataPoint> signal;

    private int maxPoints;
    private int pointsCounter = 0;
    private double samplingPeriod = 1;
    private boolean updateEnabled = false;
    private boolean forceZero = false;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);
        graph = (GraphView) rootView.findViewById(R.id.graph);
        return rootView;
    }

    public void initGraph(double minX,double maxX,double minY,double maxY,double samplingFreq) {
        
        setViewPort(minX, maxX, minY, maxY, samplingFreq);

        graph.getGridLabelRenderer().setNumHorizontalLabels(11);
        graph.getGridLabelRenderer().setPadding(50);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(true);
        graph.getGridLabelRenderer().reloadStyles();

        graph.setTitle("ECG");
        graph.getGridLabelRenderer().setHorizontalAxisTitle("Tiempo [s]");
        graph.getGridLabelRenderer().setVerticalAxisTitle("Amplitud [mV]");

        // first mSeries is a line
        signal = new LineGraphSeries<>();
        graph.addSeries(signal);
        
        // enable scaling and scrolling
        //graph.getViewport().setScalable(true);
        //graph.getViewport().setScrollable(true);
    }

    public void setViewPort(double minX,double maxX,double minY,double maxY,double samplingFreq){
        maxPoints= (int)( samplingFreq*(maxX-minX));
        samplingPeriod = 1/samplingFreq;
        // ViewPort parameters
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(minX);
        graph.getViewport().setMaxX(maxX);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(minY);
        graph.getViewport().setMaxY(maxY);
    }

    /**
     * Plot a point
     * @param dataPoint Point with x and y values
     */
    public void plot(DataPoint dataPoint){
        if(dataPoint.getX()> graph.getViewport().getMaxX(false)||dataPoint.getX()==0.0){
            if (updateEnabled) {
                refreshX();
            }
            signal.resetData(new DataPoint[]{dataPoint});
        }
        else{
            signal.appendData(dataPoint,false,maxPoints);
        }
    }

    /**
     * Append a point at a fixed period
     * @param y Value of point on y axis to append
     *
     */
    public void singleAppend(double y){

        if( pointsCounter==maxPoints||forceZero) {
            pointsCounter = 0;
            forceZero = false;
        }

        double x = pointsCounter*samplingPeriod;

        plot(new DataPoint(x, y));

        pointsCounter++;
    }

    /**
     * Allows to plot multiple points at ones
     * @param y Array of y values of samples
     * @param size Array size
     */
    public void multiAppend(double y[], int size){
        for(int i=0;i<size;i++){
            singleAppend(y[i]);
        }
    }

    /**
     *
     * @param enabled enables the X axis refreshing
     */
    public void setUpdateEnabled(boolean enabled){
        updateEnabled = enabled;
    }

    /**
     * Force redraw, starts at zero
     */
    public void forceZero(){
        forceZero=true;
    }

    /**
     * Refresh the X axis with a new range. The max of x will be de min of x. Realtime effect
     */
    private void refreshX(){
        double lastMinOfX=graph.getViewport().getMinX(false);
        double lastMaxOfX=graph.getViewport().getMaxX(false);
        double diff = lastMaxOfX-lastMinOfX;
        graph.getViewport().setMinX(lastMaxOfX);
        graph.getViewport().setMaxX(lastMaxOfX+diff);
    }

}


